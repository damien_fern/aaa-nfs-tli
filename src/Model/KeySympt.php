<?php

namespace Model;

use App\ORM\Association;

/**
 * Classe associative qui gère les associations en base entre les Keywords et les Symptomes
 */
class KeySympt extends Association
{
    protected static $table_name = "keySympt";
    protected static $ids_field_names = [
        "Model\Symptome" => "idS",
        "Model\Keywords" => "idK",
    ];
}
