<?php

namespace Model;

use App\ORM\Entity;
use App\ORM\Filter\Filter;

/*
 * Classe contenant la définition d'un Utilisateur en base de données
 */
class User extends Entity
{
    protected static $table_name = 'user';
    protected static $id_name = 'id_user';

    /**
     * Enregistre un utilisateur par son email et son password hashé
     * @return bool True si l'utilisateur a été crée, false sinon
     */
    public static function registerUser($login, $email, $hashedPwd)
    {
        $user = new User();
        $user->data["login_user"] = $login;
        $user->data["email_user"] = $email;
        $user->data["password_user"] = $hashedPwd;
        return $user->create();
    }

    /**
     * Cherche si le paramètre demandé est disponible en BDD
     */
    public static function isParamInDB($columnName, $value, $returnValue)
    {
        $s = array();
        $s = User::searchBy(
            Filter::exact($columnName, $value)
        );
        if (empty($s[0]) || empty($s[0]->data[$returnValue])) {
            return null;
        }
        return $s[0]->data[$returnValue];
    }
}
