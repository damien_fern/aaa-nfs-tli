<?php

namespace Model;

use App\ORM\Entity;

/*
 * Classe contenant les données associées aux symptômes
 */
class Symptome extends Entity
{
    protected static $table_name = 'symptome';
    protected static $id_name = 'idS';

    /**
     * Formatte le symptome pour utilisation par Twig
     * @return array
     */
    public function toRender(): array
    {
        return array(
            'desc' => $this->data['desc'],
        );
    }
}
