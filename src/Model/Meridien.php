<?php

namespace Model;

use App\ORM\Entity;

/**
 * Classe contenant la définition d'un méridien en base de données
 */
class Meridien extends Entity
{
    protected static $table_name = 'meridien';
    protected static $id_name = 'code';

    /**
     * Formatte le nom de l'élément du Méridien
     * @return string Le nom formatté
     */
    public function getElementName(): string
    {
        switch ($this->data['element']) {
            case 'e':
                $element = 'Eau';
                break;
            case 'b':
                $element = 'Bois';
                break;
            case 'f':
                $element = 'Feu';
                break;
            case 't':
                $element = 'Terre';
                break;
            case 'm':
                $element = 'Métal';
                break;
            default:
                $element = 'inconnu';
                break;
        }
        return $element;
    }

    /**
     * Définit si le méridien est du Yin
     * @return bool
     */
    public function isYin(): bool
    {
        return $this->data['yin'] === 1;
    }

    /**
     * Récupère le signe à afficher (Yin ou Yang)
     * @return string
     */
    public function isYinRender(): string
    {
        return $this->isYin() ? 'Yin' : 'Yang';
    }

    /**
     * Formatte le méridien pour utilisation avec Twig
     * @return array
     */
    public function toRender(): array
    {
        return array(
            'code' => $this->data['code'],
            'nom' => $this->data['nom'],
            'element' => $this->getElementName(),
            'yin' => $this->isYinRender(),
        );
    }
}
