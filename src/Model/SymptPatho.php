<?php

namespace Model;

use App\ORM\Association;

/**
 * Classe associative qui met en relation les symptômes et les pathologies
 */
class SymptPatho extends Association
{
    protected static $table_name = "symptPatho";
    protected static $ids_field_names = [
        "Model\Symptome" => "idS",
        "Model\Pathologie" => "idP",
    ];
}
