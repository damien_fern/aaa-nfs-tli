<?php

namespace Model;

use App\ORM\Entity;

/*
 * Classe contenant la définition d'un Mot-Clé en base de données
 */
class Keywords extends Entity
{
    protected static $table_name = 'keywords';
    protected static $id_name = 'idK';
}
