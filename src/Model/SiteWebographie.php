<?php

namespace Model;

use App\ORM\Entity;

/*
 * Classe contenant la liste de notre webographie en BDD
 */
class SiteWebographie extends Entity
{
    protected static $table_name = 'sitewebographie';
    protected static $id_name = 'id_site_webographie';
}
