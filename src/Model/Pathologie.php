<?php

namespace Model;

use App\Database;
use App\ORM\Entity;
use App\ORM\Filter\Filter;
use PDO;

/*
 * Classe contenant les données associées aux Pathologies
 */
class Pathologie extends Entity
{
    protected static $table_name = 'patho';
    protected static $id_name = 'idP';

    /**
     * Récupère les types des pathologies
     * @return array
     */
    public static function getTypes()
    {
        $pdo = Database::getInstance();
        $query = $pdo->query('
        select distinct p.type
        from patho p
        ');

//        die(var_dump($query));
        //        $query->debugDumpParams();
        $res = $query->fetchAll(PDO::FETCH_ASSOC);
        $finalArray = array();
        foreach ($res as $re) {
            $finalArray[] = $re['type'];
        }
        return $finalArray;
    }

    /**
     * Récupère le méridien pour cette pathologie
     * @return App\Model\Meridien
     */
    public function getMeridien(): Meridien
    {
        $filterMeridien = Filter::exact('code', $this->data['mer']);
        return Meridien::searchBy($filterMeridien)[0];
    }

    /**
     *  Récupère le symptome associé à cette pathologie
     *  @return array Liste des symptomes
     */
    public function getSymptome(): array
    {
        SymptPatho::doAssociation($this);

        /** @var Symptome $symptome */
        $renderSymptome = array();
        foreach ($this->associated as $symptome) {
            $renderSymptome[] = $symptome->toRender();
        }
        return $renderSymptome;
    }

    /**
     * Formatte ce méridien pour utilisation avec Twig
     * @return array
     */
    public function toRender(): array
    {
        $meridien = $this->getMeridien();
        $meridienArray = null;

        if ($meridien !== null) {
            $meridienArray = $meridien->toRender();
        }

        return array(
            'description' => $this->data['desc'],
            'meridien' => $meridienArray,
            'symptomes' => $this->getSymptome(),
            'type' => $this->data['type'],
        );
    }

    /**
     * Système de pagination des pathologies, avec tri par colonne, par sens, et page à
     * afficher
     *
     * @return array
     */
    public static function select(int $page, String $triColonne = '', String $triSens = '')
    {
        $pathologies = self::searchBy(Filter::exact(1, 1));
        $limit = 20;
        $offset = $page * $limit;
        $countPathos = count($pathologies);

        if ($offset > $countPathos) {
            $offset = $countPathos - $limit;
        }

        if ($triSens === 'ASC') {
            $sens = true;
        } else {
            $sens = false;
        }
        if ($triColonne !== '' && array_key_exists($triColonne, $pathologies[0]->data)) {
            /** @var Pathologie $one */
            /** @var Pathologie $two */
            usort($pathologies, static function ($first, $second) use ($triColonne, $sens) {
                if ($sens) {
                    return strnatcmp($first->data[$triColonne], $second->data[$triColonne]) >= 0;
                }
                return strnatcmp($first->data[$triColonne], $second->data[$triColonne]) < 0;
            });
        }
        $pathologies = array_slice($pathologies, $offset, $limit);
        return $pathologies;
    }

    /**
     *  Recherche des pathologies par mot-clé, avec pagination.
     *  Notons qu'on n'utilise pas l'ORM ici car la requête est trop complexe
     * @return array
     */
    public static function searchByKeyword(String $keyWord, int $offsetParam = 0, int $limitParam = 20)
    {
        $pdo = Database::getInstance();
        $query = $pdo->prepare('
        select p.idP, p.mer, p.type as typePatho, p.desc as descPatho,
               s.idS, s.desc descSympt,
               k.idK, k.name as nameKeyword,
               m.code, m.nom as nomMeridien, m.element, m.yin
        from patho p
        join symptPatho sp on sp.idP = p.idP
        join symptome s on s.idS = sp.idS
        join keySympt ks on ks.idS = s.idS
        join keywords k on k.idK = ks.idK
        join meridien m on m.code=p.mer
        where k.name like :keyword
        limit :limitParam
        offset :offsetParam
        ');
        $query->bindValue(':keyword', "%" . $keyWord . "%");
        $query->bindValue(':limitParam', $limitParam, PDO::PARAM_INT);
        $query->bindValue(':offsetParam', $offsetParam, PDO::PARAM_INT);

//        die(var_dump($query));
        $query->execute();
//        $query->debugDumpParams();
        $res = $query->fetchAll(PDO::FETCH_ASSOC);

        return $res;
    }

    /**
     * Recherche des pathologies par filtres
     * On n'utilise pas l'ORM car les requêtes sont trop complexes
     * @return array
     */
    public static function searchByFilters(String $meridien, String $type, String $desc)
    {
        $pdo = Database::getInstance();
        $query = $pdo->prepare('
        select p.idP, p.mer, p.type as typePatho, p.desc as descPatho
        from patho p
        where p.mer like :meridien
        and p.type like :type
        and p.desc like :desc
        ');
        $query->bindValue(':meridien', "%" . $meridien . "%");
        $query->bindValue(':type', "%" . $type . "%");
        $query->bindValue(':desc', "%" . $desc . "%");

//        die(var_dump($query));
        $query->execute();
//        $query->debugDumpParams();
        $res = $query->fetchAll(PDO::FETCH_ASSOC);

        return $res;
    }
}
