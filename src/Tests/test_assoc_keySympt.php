<?php

/*
 * Fichier permettant de donner un exemple d'utilisation de l'API générique
 * d'ORM pour la connection avec la base de données
 *
 * Teste les fonctionnalités d'association de l'outil ORM avec le cas de la lecture,
 * d'un KeySympt (tables Keywords et Symptome) en base.
 *
 * Pour lancer, executer: `php test_assoc_keySympt.php`.
 */

require __DIR__ . "/../../vendor/autoload.php";
include "common.php";

use Model\KeySympt;
use Model\Keywords;
use Model\Symptome;

/*
 * Dans la base de données, on remarque en lançant SELECT * FROM keySympt
 * le résultat suivant au début (5 premières lignes, puis plus bas) :
 *
 *      |    idK  |   idS     |
 *      _______________________
 *  1   |     3   |     61    |
 *  2   |     4   |     61    |
 *  3   |     4   |     124   |
 *  4   |     5   |     61    |
 *  5   |     5   |     64    |
 *  ... |    ...  |     ...   |
 *  80  |    87   |     61    |
 *
 * On voit donc que :
 *      * Le keyword 3 est associé uniquement au symptôme 61
 *      * Le keyword 4 est associé aux symptôme 61 et 124
 *      * Le symptôme 61 est associé aux keywords 3 ; 4 ; 5 et 87
 *
 * On peut écrire les cas de tests (en lecture) suivants :
 */

// Association simple ( un seul objet associé ) (Keywords => Symptome)
$s = Keywords::read(3);
KeySympt::doAssociation($s);

tli_assert(count($s->associated) == 1, "[ TEST 1 ] Il n'y a pas le bon nombre d'éléments associés");
$a = $s->associated[0];
tli_assert($a->data["idS"] == 61, "[ TEST 1 ] L'ID de la donnée associée n'est pas bonne");
tli_assert($a->data["desc"] == "Crampe aux 2ème, 3ème et 4ème orteils", "[ TEST 1 ] La description de la donnée
n'est pas bonne");
tli_assert(get_class($a) == "Model\Symptome", "[ TEST 1 ]La donnée associée n'est pas du bon type");

// Association multiple ( deux objets associés ) (Keywords => Symptome)
$s = Keywords::read(4);
KeySympt::doAssociation($s);

tli_assert(count($s->associated) == 2, "[ TEST 2 ] Il n'y a pas le bon nombre d'éléments associés");
tli_assert($s->associated[0]->data["idS"] == 61, "[ TEST 2 ] L'ID de la donnée 1 n'est pas bonne");
tli_assert(get_class($s->associated[0]) == "Model\Symptome", "[ TEST 2 ] Le type de donnée associé 1 n'est pas bon");
tli_assert($s->associated[1]->data["idS"] == 124, "[ TEST 2 ] L'ID de la donnée 2 n'est pas bonne");
tli_assert(get_class($s->associated[1]) == "Model\Symptome", "[ TEST 2 ] Le type de donnée associé 2 n'est pas bon");

// Association multiple dans l'autre sens (Symptome => Keywords)
$s = Symptome::read(61);
KeySympt::doAssociation($s);

tli_assert(count($s->associated) == 4, "[ TEST 3 ] Il n'y a pas le bon nombre d'éléments associés");
tli_assert($s->associated[0]->data["idK"] == 3, "[ TEST 3 ] L'ID de la donnée 1 n'est pas bonne");
tli_assert($s->associated[1]->data["idK"] == 4, "[ TEST 3 ] L'ID de la donnée 2 n'est pas bonne");
tli_assert($s->associated[2]->data["idK"] == 5, "[ TEST 3 ] L'ID de la donnée 3 n'est pas bonne");
tli_assert($s->associated[3]->data["idK"] == 87, "[ TEST 3 ] L'ID de la donnée 4 n'est pas bonne");

tli_assert(get_class($s->associated[0]) == "Model\Keywords", "[ TEST 3 ] Le type de donnée associé 1 n'est pas bon");
tli_assert(get_class($s->associated[1]) == "Model\Keywords", "[ TEST 3 ] Le type de donnée associé 2 n'est pas bon");
tli_assert(get_class($s->associated[2]) == "Model\Keywords", "[ TEST 3 ] Le type de donnée associé 3 n'est pas bon");
tli_assert(get_class($s->associated[3]) == "Model\Keywords", "[ TEST 3 ] Le type de donnée associé 4 n'est pas bon");
