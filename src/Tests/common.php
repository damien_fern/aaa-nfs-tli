<?php

/**
 * Fonction helper pour écrire des tests
 */
function tli_assert($assert, $err = "Pas de message d'erreur")
{
    if (!$assert) {
        echo "ERROR: " . $err . "\n";
    }
}
