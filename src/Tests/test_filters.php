<?php

/*
 * Fichier permettant de donner un exemple d'utilisation de l'API filters
 * pour les recherches dans la base de données
 *
 * Teste les fonctionnalités de filtrage
 *
 * Pour lancer, executer: `php test_filters.php`.
 */

require __DIR__ . "/../../vendor/autoload.php";
include_once "common.php";

use App\ORM\Filter\Filter;

$f = Filter::exact("idS", 51);
$r = $f->resolve();
tli_assert($r->sql == "idS = :0", "EXACT SQL devrait être juste");
tli_assert($r->vals[0] == 51, "EXACT vals devraient être justes");

//tli_assert($f->resolve()->sql == "idS = 51", "EXACT devrait se résoudre correctement");

Filter::$paramNum = 0;
$f = Filter::not("idS", 51);
$r = $f->resolve();
tli_assert($r->sql == "NOT idS = :0", "NOT SQL devrait se résoudre correctement");
tli_assert($r->vals[0] == 51, "NOT vals devraient être justes");

Filter::$paramNum = 0;
$f = Filter::like("desc", "Abdomen dil");
$r = $f->resolve();
tli_assert($r->sql == "desc LIKE '%:0%'", "LIKE SQL devrait se résoudre correctement");
tli_assert($r->vals[0] == "Abdomen dil", "LIKE vals devrait se résoudre correctement");

Filter::$paramNum = 0;
$f = Filter::et(
    [
        Filter::exact("idS", 51),
        Filter::like("desc", "Abdomen dil"),
    ]
);
$r = $f->resolve();
tli_assert($r->sql == "(idS = :0 AND desc LIKE '%:1%')", "AND SQL devrait se résoudre correctement");
tli_assert($r->vals[0] == 51, "AND vals 0 devrait être juste");
tli_assert($r->vals[1] == "Abdomen dil", "AND vals 1 devrait être juste");

Filter::$paramNum = 0;
$f = Filter::ou(
    [
        Filter::exact("idS", 51),
        Filter::like("desc", "Abdomen dil"),
    ]
);
$r = $f->resolve();
tli_assert($r->sql == "(idS = :0 OR desc LIKE '%:1%')", "OR SQL devrait se résoudre correctement");
tli_assert($r->vals[0] == 51, "OR vals 0 devrait être juste");
tli_assert($r->vals[1] == "Abdomen dil", "OR vals 1 devrait être juste");
