<?php

/*
 * Fichier permettant de donner un exemple d'utilisation de l'API générique
 * d'ORM pour la connection avec la base de données
 *
 * Teste les fonctionnalités CRUD de l'outil ORM avec le cas de la lecture,
 * l'ajout, la mise à jour puis le suppression d'un Symptome en base.
 *
 * Pour lancer, executer: `php test_symptome.php`.
 */

require __DIR__ . "/../../vendor/autoload.php";
include "common.php";

use App\ORM\Filter\Filter;
use Model\Symptome;

// READ
$s = Symptome::read(1);
tli_assert($s->data["idS"] == 1, "Symptome avec ID 1 devrait avoir idS à 1");
tli_assert(
    $s->data["desc"] == "Abdomen dilaté, douloureux et chaud",
    "symptome avec ID 1 devrait avoir desc à 'Abdomen dilaté, douloureux et chaud'"
);

// READ (edge case - quand on lit quelque chose qui n'existe pas en base)
$s = Symptome::read(0);
tli_assert($s == null, "Le symptôme qui n'existe pas doit être nul");

// READ multiple
$ss = Symptome::readMultiple([1, 2]);
tli_assert(count($ss) == 2, "Il doit y avoir deux symptômes dans le tableau");

tli_assert(
    get_class($ss[0]) == "Model\Symptome",
    "Le type de données associé à la donnée 1 n'est pas bon"
);
tli_assert($ss[0]->data["idS"] == 1, "L'ID associé à la donnée 1 doit être correcte");
tli_assert(
    $ss[0]->data["desc"] == "Abdomen dilaté, douloureux et chaud",
    "La description associée à la donnée 1 doit être correcte"
);

tli_assert(
    get_class($ss[1]) == "Model\Symptome",
    "Le type de données associé à la donnée 2 n'est pas bon"
);
tli_assert($ss[1]->data["idS"] == 2, "L'ID associé à la donnée 2 doit être correcte");
tli_assert(
    $ss[1]->data["desc"] == "Abdomen flasque",
    "La descritpion associée à la donnée 2 doit être correcte"
);

// SEARCH BY
$s = Symptome::searchBy(
    Filter::ou([
        Filter::exact("idS", 1),
        Filter::exact("idS", 2),
    ])
);

tli_assert(count($s) == 2, "Il doit y avoir deux symptômes dans les résultats de search_by");
tli_assert($s == $ss, "Les résultats de search_by et de read_multiple doivent être identiques dans ce cas");

// CREATE
$s = new Symptome();
$s->data["desc"] = "Mal de tête très violent quand on code du Java";
tli_assert($s->create(), "Insertion du symptôme échoué");

// UPDATE
$idS = $s->data["idS"];
$s->data["desc"] = "Mal de tête violent quand on code du Java";
tli_assert($s->update(), "Mise à jour du symptôme échoué");

// (READ après UPDATE pour vérifier que les données sont bien mises à jour)
$ss = Symptome::read($idS);
tli_assert($ss != null, "Lecture après MàJ retourne un null");
tli_assert($ss->data["idS"] == $idS);
tli_assert(
    $ss->data["desc"] == "Mal de tête violent quand on code du Java",
    "Lecture après mise à jour n'a pas les bonnes valeurs"
);

// DELETE
tli_assert(Symptome::delete($idS), "Suppression du symptôme");
