<?php

namespace Controller;

use App\Controller;
use App\ORM\Filter\Filter;
use Model\SiteWebographie;

/**
 * Controller des routes par défaut
 * @package Controller
 */
class DefaultController extends Controller
{

    /**
     * Effectue le rendu de l'accueil et du flux RSS
     * @return \App\Response\Response
     */
    public function indexAction()
    {
        $url = "../test.xml"; /* insérer ici l'adresse du flux RSS de votre choix */
        $rss = simplexml_load_file($url);

        return $this->render("accueil.html.twig", ["rss" => $rss->channel]);
    }

    /**
     * Effectue le rendu de l'index
     * @return \App\Response\Response
     */
    public function nameAction($name)
    {
        return $this->render("index.html.twig", [
            "name" => $name,
        ]);
    }

    /**
     * Effectue le rendu de la page "A propos"
     * @return \App\Response\Response
     */
    public function renderPageAPropos()
    {
        $webographie = SiteWebographie::searchBy(Filter::exact(1, 1));

        $res = [];
        foreach ($webographie as $key => $site) {
            $res[$key] = $site->data;
        }
        return $this->render('apropos.html.twig', ['sites' => $res]);
    }
}
