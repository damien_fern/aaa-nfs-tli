<?php

namespace Controller;

use App\Controller;
use App\Response\HTTPJsonResponse;
use App\Response\JsonResponse;
use App\Router\RouterException;

/**
 * Controller qui gère l'API REST pour les modules suivants :
 * * symptome
 * * keywords
 * * meridien
 * * pathologie
 */
class RestController extends Controller
{
    /*
     * Constructeur par défaut du RestController
     */
    public function __construct($request, $router)
    {
        parent::__construct($request, $router);

        /*
         * Liste de modules accessibles dans ce contrôleur
         */
        $this->modules = [
            "symptome" => "Model\Symptome",
            "keywords" => "Model\Keywords",
            "meridien" => "Model\Meridien",
            "pathologie" => "Model\Pathologie",
        ];
    }

    /**
     * Dispatch la requête vers le module concerné pour un appel sans ID
     *
     * @return App\Response\JsonResponse|App\Response\HttpJsonResponse une Response appropriée, ou soulève une exception
     */
    public function dispatch($module)
    {
        $this->checkModule($module);
        $class = $this->modules[$module];
        $method = $this->request->getServer()["REQUEST_METHOD"];
        switch ($method) {
            case 'GET':
                $rep = $class::readAll();
                $rep = array_map(function ($x) {return $x->data;}, $rep);
                return new JsonResponse($rep);
                break;
            case 'POST':
                $instance = new $class();
                foreach ($_POST as $k => $v) {
                    $instance->data[$k] = $v;
                }
                if (!$instance->create()) {
                    throw new RouterException("Could not create " . $module);
                } else {
                    return new HttpJsonResponse($instance->data, 201);
                }
                break;
            default:
                throw new RouterException("Wrong HTTP Method for module "
                    . $module . ", could not handle " . $method);
                break;
        }
    }

    /**
     * Dispatch la requête vers le module concerné pour un appel avec ID
     *
     * @return App\Response\JsonResponse|App\Response\HttpJsonResponse une Response appropriée, ou soulève une exception
     */
    public function dispatchWithID($module, $id)
    {
        $this->checkModule($module);
        $class = $this->modules[$module];
        $method = $this->request->getServer()["REQUEST_METHOD"];
        switch ($method) {
            case 'GET':
                $rep = $class::read($id);
                if ($rep == null) {
                    throw new RouterException("No " . $module . " found for id " . $id);
                }
                return new JsonResponse($rep->data);
                break;
            case 'PUT':
                parse_str(file_get_contents('php://input'), $_PUT);
                $instance = $class::read($id);
                foreach ($_PUT as $k => $v) {
                    $instance->data[$k] = $v;
                }
                if (!$instance->update()) {
                    throw new RouterException("Failed update on " . $module . " for id " . $id);
                }
                return new JsonResponse($instance->data);
                break;
            case 'DELETE':
                if ($class::delete($id)) {
                    return new HttpJsonResponse(null, 204);
                } else {
                    throw new RouterException("Failed to delete on " . $module . " for id " . $id);
                }
                break;
            default:
                throw new RouterException("Wrong HTTP Method for module "
                    . $module . ", could not handle " . $method);
                break;
        }
    }

    /*
     * Vérifie que le module d'API existe, sinon, on lance une RouterException
     * pour arrêter le traitement de la requête
     */
    public function checkModule($module)
    {
        if (!array_key_exists($module, $this->modules)) {
            throw new RouterException("No API for module " . $module);
        }
    }

}
