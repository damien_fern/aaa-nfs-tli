<?php

namespace Controller;

use App\Controller;
use App\ORM\Filter\FilterExact;
use App\Response\JsonResponse;
use Model\Meridien;
use Model\Pathologie;

/**
 * Controller pour les routes qui concernent les pathologies
 * @package Controller
 */
class PathologieController extends Controller
{
    /*
     *  Effectue le rendu de la page de pathologie
     */
    public function renderPage()
    {
        $pageGet = isset($this->request->getGet()['page']) && ((int) $this->request->getGet()['page']) > 1 ? $this->request->getGet()['page'] : 1;
        $page = $pageGet - 1;

        $uri = $this->request->getServer()['REQUEST_URI'];
        if (strpos($uri, 'page=') === false) {
            $inter = '&';
            if (strpos($uri, '?') === false) {
                $inter = '?';
            }
            $uri .= $inter . 'page=' . $pageGet;
        }

        $triColonneGet = $this->request->getGet()['triColonne'] ?? '';
        $triSensGet = $this->request->getGet()['triSens'] ?? '';
        $pattern = "/^(\/.*)(\?|\&)(page=)(\d*)(.*)$/";
        $urlNext = preg_replace_callback(
            $pattern,
            static function ($matches) {
                // comme d'habitude : $matches[0] représente la valeur totale
                // $matches[1] représente la première parenthèse capturante
                return $matches[1] . $matches[2] . $matches[3] . ($matches[4] + 1) . $matches[5];
            },
            $uri);
        $urlPrevious = preg_replace_callback(
            $pattern,
            static function ($matches) {
                // comme d'habitude : $matches[0] représente la valeur totale
                // $matches[1] représente la première parenthèse capturante
                return $matches[1] . $matches[2] . $matches[3] . ($matches[4] - 1 === 0 ? 1 : $matches[4] - 1) . $matches[5];
            },
            $uri);
        $pathologies = Pathologie::select($page, $triColonneGet, $triSensGet);
        $meridiens = Meridien::searchBy(new FilterExact(1, 1));
        $types = Pathologie::getTypes();

        return $this->render('pathologies.html.twig', array(
            'pathologies' => $pathologies,
            'meridiens' => $meridiens,
            'types' => $types,
            'page' => $pageGet,
            'urlNext' => $urlNext,
            'urlPrevious' => $urlPrevious,
        ));
    }

    /*
     * Effectue le rendu d'une pathologie de façon précisé
     */
    public function renderOne($idPatho)
    {
        $pathologie = Pathologie::read($idPatho);

        if ($pathologie === null) {
            return $this->redirect('pathologies'); // TODO : CREATE NOT FOUND ROUTE
        }
        return $this->render('detailPathologies.html.twig', [
            'pathologie' => $pathologie->toRender(),
        ]);
    }

    /*
     * Effectue le rendu de la page d'ajout d'une pathologie
     */
    public function renderAdd()
    {
        return $this->render("addPatho.html.twig");
        //Todo : Enregistrer l'ajout des pathologies
    }

    /**
     * Route qui trouve une pathologie en cherchant par Keyword
     * @return App\Response\JsonReponse
     */
    public function findPathoByKeyword()
    {
        $res = Pathologie::searchByKeyword($this->request->getpost()['rechercheValue']);
        $response = new JsonResponse($res);
        return $response;
    }

    /**
     * Route qui trouve une pathologie après recherche par Filtres
     * @return App\Response\JsonResponse
     */
    public function findPathoByFilters()
    {
        $meridienGet = $this->request->getGet()['data']['mer'] ?? '';
        $typeGet = $this->request->getGet()['data']['type'] ?? '';
        $descGet = $this->request->getGet()['data']['desc'] ?? '';
        $res = Pathologie::searchByFilters($meridienGet, $typeGet, $descGet);

//        die(var_dump( $this->request->getGet()['data'] ));
        $response = new JsonResponse($res);

        return $response;
    }
}
