<?php

namespace Controller;

use App\Controller;
use App\Request;
use App\Response;
use Model\User;

/**
 * Controller pour l'authentification utilisateur
 * @package Controller
 */
class CompteController extends Controller
{
    public $loginErrors = array();
    public $emailErrors = array();
    public $passwordErrors = array();

    /**
     * Redirige vers la page de connexion / inscription
     * @return \App\Response\RedirectResponse|\App\Response\Response
     */
    public function renderPage()
    {
        if (!self::isConnected()) {
            return $this->render('login.html.twig', []);
        }
        return $this->redirect('index');
    }

    /**
     * Démarre l'action d'inscription de l'utilisateur
     * Si un utilisateur est déjà connecté, renvoie vers la page d'accueil
     * Sinon si toutes info renseignées sont correctes, crée l'utilisateur et le
     * connecte puis renvoie page d'accueil
     * Si les info ne sont pas correctes, des messages d'erreur appropriés sont
     * affichés en dessous des champs du formulaire
     * @return \App\Response\Response
     */
    public function registerAction()
    {
        if (self::isConnected()) {
            return $this->redirect("index", []);
        } elseif ($this->register()) {
            $login = $this->request->getPost()['loginInscription'];
            $password = $this->request->getPost()['passwordInscription'];
            $this->login($login, $password);
            return $this->redirect("index", []);
        } else {
            $errors = array(
                "login" => $this->loginErrors,
                "email" => $this->emailErrors,
                "password" => $this->passwordErrors,
            );
            $parameters = array("errorsRegister" => $errors);
            if (isset($this->request->getPost()['loginInscription'])
                && !empty($this->request->getPost()['loginInscription'])
            ) {
                $parameters["loginRegister"] = $this->request->getPost()['loginInscription'];
            }
            if (isset($this->request->getPost()['emailInscription'])
                && !empty($this->request->getPost()['emailInscription'])
            ) {
                $parameters["emailRegister"] = $this->request->getPost()['emailInscription'];
            }
            return $this->render("login.html.twig", $parameters);
        }
    }

    /**
     * Démarre l'action de connexion de l'utilisateur
     * Si les info sont valides, redirige vers la page d'accueil
     * Sinon affiche des messages d'erreur en dessous des champs du formulaire
     * @return Response\RedirectResponse|Response\Response
     */
    public function loginAction()
    {
        if (self::isConnected()) {
            return $this->redirect("index", []);
        } elseif (isset($this->request->getPost()['loginConnexion'])
            && isset($this->request->getPost()['passwordConnexion'])
            && !empty($this->request->getPost()['loginConnexion'])
            && !empty($this->request->getPost()['passwordConnexion'])
        ) {
            $login = $this->request->getPost()['loginConnexion'];
            $password = $this->request->getPost()['passwordConnexion'];
            if ($this->login($login, $password)) {
                return $this->redirect("index", []);
            } else {
                $errorsLogin = array("login" => $this->loginErrors, "password" => $this->passwordErrors);
                return $this->render("login.html.twig", array("errorsLogin" => $errorsLogin, "loginLogin" => $login));
            }
        } else {
            $errorsLogin = array();
            $login = "";
            if (!isset($this->request->getPost()['loginConnexion'])
                || empty($this->request->getPost()['loginConnexion'])
            ) {
                $errorsLogin["login"] = array("Veuillez remplir le login");
            } else {
                $login = $this->request->getPost()['loginConnexion'];
            }
            if (!isset($this->request->getPost()['passwordConnexion'])
                || empty($this->request->getPost()['passwordConnexion'])
            ) {
                $errorsLogin["password"] = array("Veuillez remplir le mot de passe");
            }
            return $this->render("login.html.twig", array("errorsLogin" => $errorsLogin, "loginLogin" => $login));
        }
    }

    /**
     * Vérifie Vérifie que le couple login mdp correspond à un utilisateur en BDD
     * Si l'utilisateur est trouvé, la session est instanciée
     * @param $login
     * @param $password
     * @return bool 
     */
    public function login($login, $password)
    {
        $this->resetErrorArrays();
        $storedHash = User::isParamInDB("login_user", $login, "password_user");
        if (empty($storedHash)) {
            $this->loginErrors["incorrectLogin"] = "Login incorrect";
            return false;
        }
        if (password_verify($password, $storedHash)) {
            if (self::isConnected()) {
                $this->logout();
            }
            $_SESSION['user'] = $login;
            $_SESSION['isConnected'] = true;
        } else {
            $this->passwordErrors["incorrectPwd"] = "Mot de passe incorrect";
            return false;
        }
        return true;
    }

    /**
     * Démarre l'action de déconnexion de l'utilisateur
     * Déinstancie toutes les variables de session puis redirige sur la page d'accueil
     * @return \App\Response\RedirectResponse|\App\Response\Response
     */
    public function logout()
    {
        session_unset();
        //session_destroy();
        return $this->redirect('index');
    }

    /**
     * Vérifie si les informations rentrées par l'utilisateur respectes les conditions
     * Si validé, crée l'utilisateur en BDD et return true
     * Sinon return false
     * @return bool
     */
    public function register()
    {
        $this->resetErrorArrays();
        $ret = false;
        if (isset($this->request->getPost()['loginInscription'])
            && isset($this->request->getPost()['emailInscription'])
            && isset($this->request->getPost()['emailInscription'])
        ) {
            $login = $this->request->getPost()['loginInscription'];
            $email = $this->request->getPost()['emailInscription'];
            $password = $this->request->getPost()['passwordInscription'];
            if (!$this->checkLogin($login)) { //Vérif si le login est valide
                $ret = true;
            }
            $storedHash = User::isParamInDB("login_user", $login, "password_user");
            if (!empty($storedHash)) {
                $this->loginErrors["usedLogin"] = "Ce login est déjà utilisé par un autre utilisateur";
                $ret = true;
            }
            if (!$this->checkEmail($email)) { //Vérif si l'email est valide
                $ret = true;
            }
            $storedHash = User::isParamInDB("email_user", strtolower($email), "password_user");
            if (!empty($storedHash)) {
                $this->emailErrors["usedEmail"] = "Cet email est déjà utilisé par un autre utilisateur";
                $ret = true;
            }
            if (!$this->checkPasswordComplexity($password)) { //Vérif si le mdp est valide
                $ret = true;
            }
            if ($ret) {
                return false;
            }
            $hash = $this->hashString($password);
            return User::registerUser($login, strtolower($email), $hash);
        }
        return false;
    }

    /**
     * Si l'utilisateur est connecté return true
     * sinon false
     * @return bool
     */
    public static function isConnected()
    {
        if (isset($_SESSION['user']) && !empty($_SESSION['user'])) {
            return true;
        }
        return false;
    }

    /**
     * Réinitialise les tableaux de log
     */
    private function resetErrorArrays()
    {
        $this->loginErrors = null;
        $this->emailErrors = null;
        $this->passwordErrors = null;
    }

    /**
     * Hash la string passée en param avec l'algo ARGON2I
     * @param $string
     * @return bool|string
     */
    private function hashString($string)
    {
        return password_hash($string, PASSWORD_ARGON2I);
    }

    /**
     * Vérifie que les conditions de validation du login en param soient respectées
     * @param $login
     * @return bool
     */
    private function checkLogin($login)
    {
        if (empty($login) || strlen($login) < 6) {
            $this->loginErrors["loginSize"] = "Le login doit mesurer au moins 6 caractères";
            return false;
        }
        return true;
    }

    /**
     * Vérifie que les conditions de validation de l'email en param soient respectées
     * @param $email
     * @return bool
     */
    private function checkEmail($email)
    {
        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return true;
        } else {
            $this->emailErrors["emailFormat"] = "Le format de l'email n'est pas valide";
            return false;
        }
    }

    /**
     * Vérifie que les conditions de validation du mot de passe en param soient respectées
     * @param $password
     * @return bool
     */
    private function checkPasswordComplexity($password)
    {
        $errors_init = $this->passwordErrors;

        if (empty($password) || strlen($password) < 8) {
            $this->passwordErrors["pwdSize"] = "Le mot de passe doit mesurer au moins 8 caractères";
        }

        if (!preg_match("/\d/", $password)) {
            $this->passwordErrors["pwdNumber"] = "Le mot de passe doit contenir au moins un chiffre";
        }

        if (!preg_match('/[a-z]+/', $password) || !preg_match('/[A-Z]+/', $password)) {
            $this->passwordErrors["pwdUpperAndLower"] = "Le mot de passe doit
                contenir au moins une lettre en minucule et une lettre en majuscule";
        }

        if (!preg_match('/[\'^£$%&*()}{@#~?><>,|=_+¬-]/', $password)) {
            $this->passwordErrors["pwdSpecialCaract"] = "Le mot de passe doit contenir au moins un caractère spécial";
        }

        return ($this->passwordErrors == $errors_init);
    }
}
