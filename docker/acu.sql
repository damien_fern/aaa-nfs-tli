DROP DATABASE IF EXISTS `acu`;
CREATE DATABASE `acu`;
USE `acu`;


DROP TABLE IF EXISTS `keysympt`;
CREATE TABLE `keysympt` (
  `idK` int(11) NOT NULL,
  `idS` int(11) NOT NULL,
  PRIMARY KEY (`idK`,`idS`)
);

DROP TABLE IF EXISTS `keywords`;
CREATE TABLE `keywords` (
  `idK` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id du mot clef',
  `name` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'mot clef',
  PRIMARY KEY (`idK`)
);

DROP TABLE IF EXISTS `meridien`;
CREATE TABLE `meridien` (
  `code` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'code du méridien',
  `nom` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'nom',
  `element` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'élément',
  `yin` tinyint(1) NOT NULL COMMENT 'vrai si yin',
  PRIMARY KEY (`code`),
  KEY `element` (`element`)
);

DROP TABLE IF EXISTS `patho`;
CREATE TABLE `patho` (
  `idP` int(11) NOT NULL AUTO_INCREMENT,
  `mer` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'code du méridien',
  `type` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `desc` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'description de la pathologie',
  PRIMARY KEY (`idP`),
  KEY `code` (`mer`)
);

DROP TABLE IF EXISTS `sitewebographie`;
CREATE TABLE `sitewebographie` (
  `id_site_webographie` int(11) NOT NULL AUTO_INCREMENT,
  `titre` text NOT NULL,
  `url` text NOT NULL,
  `utilisation` text NOT NULL,
  PRIMARY KEY (`id_site_webographie`)
) AUTO_INCREMENT=3;

INSERT INTO `sitewebographie` (`id_site_webographie`, `titre`, `url`, `utilisation`) VALUES
(1,	'cssscript',	'www.cssscript.com',	'Exemples de bonnes pratiques de responsivité'),
(2,	'w3schools',	'www.w3schools.com',	'Verifications des bonnes pratiques');

DROP TABLE IF EXISTS `symptome`;
CREATE TABLE `symptome` (
  `idS` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id du symptome',
  `desc` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'description du symptôme',
  PRIMARY KEY (`idS`)
);

DROP TABLE IF EXISTS `symptpatho`;
CREATE TABLE `symptpatho` (
  `idS` int(11) NOT NULL,
  `idP` int(11) NOT NULL,
  `aggr` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Vrai si symptôme d''aggravation',
  PRIMARY KEY (`idS`,`idP`)
);

DROP TABLE IF EXISTS user;
CREATE TABLE user (
  id_user int(11) NOT NULL AUTO_INCREMENT,
  email_user text NOT NULL,
  login_user text NOT NULL,
  password_user text NOT NULL,
  PRIMARY KEY (id_user)
);