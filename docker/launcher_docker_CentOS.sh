#!/bin/bash

#####################################################################################
#   But du script :                                                                 #
#       - Modification des fichiers os pour installation de Docker                  #
#       - Configuration de docker via docker file                                   #
#                                                                                   #
#               // ! \\ : il faut lancer le script en root                          #
#                                                                                   #
#####################################################################################
# Crontab : script au lancement de la vm                                            #
#####################################################################################

# Suppresion ancienne version docker
sudo yum remove docker \
                  docker-client \
                  docker-client-latest \
                  docker-common \
                  docker-latest \
                  docker-latest-logrotate \
                  docker-logrotate \
                  docker-engine



# ~~~~~~~~ COMMENTER SI DOCKER EST DEJA INSTALLE ~~~~~~~~

sudo yum install -y yum-utils \
  device-mapper-persistent-data \
  lvm2

sudo yum-config-manager \
    --add-repo \
    https://download.docker.com/linux/centos/docker-ce.repo

yum update
sudo yum install docker-ce docker-ce-cli containerd.io


sudo systemctl enable docker

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~