# AAA Not From Scratch TLI

[![pipeline status](https://gitlab.com/damien_fern/aaa-nfs-tli/badges/master/pipeline.svg)](https://gitlab.com/damien_fern/aaa-nfs-tli/commits/master)

Projet TLI 4IRC CPE Lyon

Equipe :

* Damien Fernandes
* Florian Gros
* Nathan Kozlowski
* Benjamin Lahouze
* Tim Mallet
* Olivier Pinon
* Dylan Sinault

## Installation

`git clone` ce projet  
`composer install` pour installer les dépendances (Outil de templating: Twig)

Point d'entrée unique du site: `public/index.php`

## BDD

![Schema bdd](bd-acu.png)


## Architecture de l'Infra

![Schema Infra](Topo_Infra_Dock.png)
