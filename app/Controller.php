<?php

namespace App;

use App\Database;
use App\Response\JsonResponse;
use App\Response\RedirectResponse;
use App\Response\Response;
use App\Router\Router;
use Twig\Environment as TwigEnvironment;
use Twig\Loader\FilesystemLoader as TwigFilesystemLoader;

/**
 * Class Controller
 * @package App
 */
class Controller
{
    /**
     * @var Request
     */
    protected $request;

    /**
     * @var Router
     */
    private $router;

    /**
     * @var TwigEnvironment
     */
    private $twig;

    /**
     * @var Database
     */
    private $database;

    /**
     * Constructeur par défaut d'un Controller
     * @param Request $request
     * @param Router $router
     */
    public function __construct(Request $request, Router $router)
    {
        session_start();
        $this->request = $request;
        $this->router = $router;
        $this->database = Database::getInstance();

        // On instancie le loader de twig en lui précisant dans quel dossier se trouvera nos vues
        $loader = new TwigFilesystemLoader(__DIR__ . '/../src/View');
        $env = array(
            'cache' => __DIR__ . '/../var/cache',
            'debug' => false,
        );

        if ($request->getEnv("APP_ENV") === "dev") {
            $env["debug"] = true;
        }

        // On instancie l'environnement de twig, qui nous permettra de générer nos vues
        $this->twig = new TwigEnvironment($loader, $env);
    }

    /**
     * Raccourci pour effectuer une redirection sur une autre route
     * @param string $routeName
     * @param array $args
     * @return RedirectResponse
     */
    protected function redirect($routeName, $args = [])
    {
        // On récupère la route par son nom
        $route = $this->router->getRoute($routeName);
        // On génère l'url, $args contient les valeurs de chaque paramètre de la route
        $url = $route->generateUrl($args);
        // On renvoie un objet RedirectResponse
        return new RedirectResponse($url);
    }

    /**
     * @param string $filename
     * @param array $data
     * @return Response
     */
    protected function render($filename, $data = [])
    {
        // On charge notre vue
        $this->twig->addGlobal('session', $_SESSION);
        $this->twig->addExtension(new \Twig\Extension\DebugExtension());
        $view = $this->twig->load($filename);
        // On récupère le contenu de la vue en lui passant nos données pour que la vue puisse les exploiter
        $content = $view->render($data);
        // On renvoie un objet Response
        return new Response($content);
    }

    /**
     * Raccourci pour renvoyer une réponse HTTP 200 avec du contenu JSON
     * @param mixed $data
     * @return JsonResponse
     */
    protected function json($data)
    {
        // On renvoie un objet JsonResponse
        return new JsonResponse($data);
    }

    /**
     * Récupère la base de données associée à ce controller
     * @return Database
     */
    protected function getDatabase()
    {
        return $this->database;
    }
}
