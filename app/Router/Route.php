<?php

namespace App\Router;

use App\Request;
use App\Response\ResponseInterface;

/**
 * Class Route
 * @package App
 */
class Route
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $path;

    /**
     * @var array
     */
    private $parameters;

    /**
     * @var array
     */
    private $defaults = [];

    /**
     * @var string
     */
    private $controller;

    /**
     * @var string
     */
    private $action;

    /**
     * @var array
     */
    private $args;

    /**
     * Route constructor.
     * @param string $name Nom de la route
     * @param string $path Chemin de la route
     * @param array $parameters Paramètres
     * @param string $controller Controller associé
     * @param string $action Action de la route
     */
    public function __construct($name, $path, array $parameters, $controller, $action, $defaults = [])
    {
        $this->name = $name;
        $this->path = $path;
        $this->parameters = $parameters;
        $this->controller = $controller;
        $this->action = $action;
        $this->defaults = $defaults;
    }

    /**
     * Appelle cette route
     * @param Request $request
     * @param Router $router
     * @return ResponseInterface
     */
    public function call(Request $request, Router $router)
    {
        $controller = $this->controller;
        // On instancie dynamiquement le contrôleur
        $controller = new $controller($request, $router);
        // call_user_func_array permet d'appeler une méthode
        // (ou une fonction, cf la doc) d'une classe et de lui passer des arguments
        return call_user_func_array([$controller, $this->action], $this->args);
    }

    /**
     * Vérifie si la route matche avec l'URI
     * @param string $requestUri
     * @return bool
     */
    public function match($requestUri)
    {
        // On génère un nouveau chemin en remplaçant les paramètres par des regexp
        $path = preg_replace_callback("/:(\w+)/", [$this, "parameterMatch"], $this->path);
        // On échappe chaque "/" pour que notre regexp puisse reconnaître le "/"
        $path = str_replace("/", "\/", $path);
        // Si notre requpete actuelle ne correspond pas à la regexp alons on renvoie false
        if (!preg_match("/^$path(\?.*)*$/i", $requestUri, $matches)) {
            return false;
        }
        // Sinon on remplit notre tableau d'arguments avec les valeurs de chaque paramètre de notre route
        $this->args = array_slice($matches, 1);
        $defaultsArgs = array_keys($this->defaults);
        foreach ($this->args as $key => &$value) {
            $index = array_search($key, $defaultsArgs);
            if ($index !== false && $value === "") {
                $value = $this->defaults[$defaultsArgs[$index]];
            }
        }
        return true;
    }

    /**
     * Retourne une regex pour matcher les paramètres
     * @param $match
     * @return string
     */
    private function parameterMatch($match)
    {
        // Si nous avons bien définie notre paramètre alors on renvoie la regexp associé
        if (isset($this->parameters[$match[1]])) {
            return sprintf("(%s)", $this->parameters[$match[1]]);
        }
        // Sinon on renvoie une regexp par défaut
        return '([^/]+)';
    }

    /**
     * Génère l'URL de la route associée
     * @param $args
     * @return string
     */
    public function generateUrl($args)
    {
        // On remplace chaque paramètre du chemin par les arguments transmis
        $url = str_replace(array_keys($args), $args, $this->path);
        // On supprime les ":"
        $url = str_replace(":", "", $url);
        return $url;
    }

    /**
     * Récupère le nom de la route
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Récupère le chemin de la route
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Récupère les paramètres de la route
     * @return array
     */
    public function getParameters()
    {
        return $this->parameters;
    }

    /**
     * Récupère le controller associé à la route
     * @return string
     */
    public function getController()
    {
        return $this->controller;
    }

    /**
     * Récupère l'action associée
     * @return string
     */
    public function getAction()
    {
        return $this->action;
    }
}
