<?php

namespace App;

/*
 * Récupère une instance d'un PDO en se connectant à la base de données.
 * Les informations de connexion sont à rentrer dans les variables
 * d'environnement du système ; on peut aussi utiliser un fichier .env
 */
class Database
{
    private $pdo;
    private static $instance = null;

    /**
     * Constructeur de la classe
     *
     * @param void
     * @return void
     */
    private function __construct()
    {
        // Config DB connection (avec valeurs par défaut si on ne trouve rien)
        $db_server = isset($_ENV["DB_SERVER"]) ? $_ENV["DB_SERVER"] : 'mysql';
        $db_host = isset($_ENV["DB_HOST"]) ? $_ENV["DB_HOST"] : '127.0.0.1';
        $db_port = isset($_ENV["DB_PORT"]) ? $_ENV["DB_PORT"] : '3306';
        $db_name = isset($_ENV["DB_NAME"]) ? $_ENV["DB_NAME"] : 'acu';
        $db_charset = isset($_ENV["DB_CHARSET"]) ? $_ENV["DB_CHARSET"] : 'utf8mb4';

        // Base def pour username & password, TODO: load from .env
        $db_username = isset($_ENV["DB_USER"]) ? $_ENV["DB_USER"] : "root";
        $db_passwd = isset($_ENV["DB_PASSWORD"]) ? $_ENV["DB_PASSWORD"] : "test";
        $db_dsn = $db_server
            . ':host='
            . $db_host
            . ';port='
            . $db_port
            . ';dbname='
            . $db_name
            . ';charset='
            . $db_charset;

        $this->pdo = new \PDO($db_dsn, $db_username, $db_passwd);
    }

    /**
     * Récupère le PDO associé à cette requête
     * @return PDO
     */
    public function getPdo()
    {
        return $this->pdo;
    }

    /**
     * Récupère l'instance PDO du Singleton de cette classe
     * @return PDO
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new Database();
        }
        return self::$instance->getPdo();
    }
}
