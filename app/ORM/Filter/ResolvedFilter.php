<?php

namespace App\ORM\Filter;

/*
 * Classe contenant le résultat de la résolution d'un filtre
 */
class ResolvedFilter
{
    /*
     * Crée un résultat à partir de la syntaxe SQL et des valeurs à binder
     */
    public function __construct($sql, $vals)
    {
        $this->sql = $sql;
        $this->vals = $vals;
    }

    /**
     * Bind la query passée en paramètre avec les valeurs connues du filtre
     * @param &$query référence sur la query PDO à préparer
     */
    public function bindValues(&$query)
    {
        foreach ($this->vals as $k => $v) {
            $query->bindValue(":" . $k, $v);
        }
    }

}
