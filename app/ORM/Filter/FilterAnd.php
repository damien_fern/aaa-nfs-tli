<?php

namespace App\ORM\Filter;

use App\ORM\Filter\Filter;
use App\ORM\Filter\ResolvedFilter;

/*
 * Filtre permettant de résoudre plusieurs conditions avec une contrainte AND
 */
class FilterAnd extends Filter
{
    protected function __construct($filters)
    {
        $this->filters = $filters;
    }

    /**
     * Résouds ce filtre
     * @return App\ORM\Filter\ResolvedFilter Le ResolvedFilter associé
     */
    public function resolve(): ResolvedFilter
    {
        $vals = [];
        $sql = "(" . implode(" AND ", array_map(function ($x) use (&$vals) {
            $resolved = $x->resolve();
            $vals = array_merge($vals, $resolved->vals);
            return $resolved->sql;
        }, $this->filters)) . ")";

        return new ResolvedFilter(
            $sql,
            $vals
        );
    }
}
