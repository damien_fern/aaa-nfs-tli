<?php

namespace App\ORM\Filter;

use App\ORM\Filter\Filter;

/*
 * Filtre permettant de résoudre une condition avec
 * une contrainte d'égalité parfaite
 */
class FilterExact extends Filter
{
    /**
     * Résouds ce filtre
     * @return App\ORM\Filter\ResolvedFilter Le ResolvedFilter associé
     */
    public function resolve(): ResolvedFilter
    {
        $vals = $this->setupParams([$this->value]);
        $sql = $this->field . " = :" . array_keys($vals)[0];
        return new ResolvedFilter($sql, $vals);
    }
}
