<?php

namespace App\ORM\Filter;

use App\ORM\Filter\Filter;
use App\ORM\Filter\ResolvedFilter;

/*
 * Filtre permettant de résoudre une condition avec une contrainte
 * d'inégalité
 */
class FilterNot extends Filter
{
    /**
     * Résouds ce filtre
     * @return App\ORM\Filter\ResolvedFilter Le ResolvedFilter associé
     */
    public function resolve(): ResolvedFilter
    {
        $vals = $this->setupParams([$this->value]);
        $sql = "NOT " . $this->field . " = :" . array_keys($vals)[0];
        return new ResolvedFilter($sql, $vals);
    }
}
