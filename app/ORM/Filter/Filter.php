<?php

namespace App\ORM\Filter;

use App\ORM\Filter\FilterAnd;
use App\ORM\Filter\FilterExact;
use App\ORM\Filter\FilterLike;
use App\ORM\Filter\FilterNot;
use App\ORM\Filter\FilterOr;
use App\ORM\Filter\ResolvedFilter;

/**
 * Implémentation basique d'un système de filtre générique pour la recherche
 *
 * On aura plusieurs filtres qui héritent de la classe Filter,
 * chaque filtre expose une méthode `resolve` (abstraite dans Filter), qui
 * permet d'écrire la syntaxe SQL nécéssaire à la requête
 *
 * searchBy dans la classe Entity se chargera de créer la requête entière.
 */
abstract class Filter
{
    public static $paramNum = 0;
    /*
     * Constructeur par défaut de la classe Filter.
     *
     * Un filter a besoin de :
     *      * Field: string (le nom de champ en base de données)
     *      * Value: mixed (la valeur pour filtrer)
     */
    public function __construct(string $field, $value)
    {
        $this->field = $field;
        $this->value = $value;
    }

    /**
     * Fonction helper permettant de formatter la valeur correctement en SQL
     * @return string
     */
    protected function formattedValue(): string
    {
        switch (gettype($this->value)) {
            case "string":
                return "'" . $this->value . "'";
            default:
                return $this->value;
        }
    }

    /**
     * Associe les paramètres avec un numéro qui leur correspond
     * @return array le tableau paramétré
     */
    protected function setupParams(array $vals): array
    {
        $ret = [];
        foreach ($vals as $v) {
            $ret[Filter::$paramNum] = $v;
            Filter::$paramNum += 1;
        }
        return $ret;
    }

    /**
     * Fonction qui doit être implémentée dans chaque objet qui hérite
     * de Filter, résouds le Filtre en commande SQL utilisable par MySQL
     *
     * @return App\ORM\Filter\ResolvedFilter Le ResolvedFIlter qui correspond à ce filtre
     */
    abstract public function resolve(): ResolvedFilter;

    /**
     * Retourne un filtre "Exact"
     * @return App\ORM\Filter\FilterExact Le filtre "FilterExact" généré par les paramètres donnés
     */
    public static function exact(string $field, $value): FilterExact
    {
        return new FilterExact($field, $value);
    }

    /**
     * Retourne un filtre "Like"
     * @return App\ORM\Filter\FilterLike Le filtre "FilterLike" généré par les paramètres donnés
     */
    public static function like(string $field, $value): FilterLike
    {
        return new FilterLike($field, $value);
    }

    /**
     * Retourne un filtre "And" (nommé "Et" parce que PHP ne veut pas
     * qu'on appelle nos fonctions "and")
     *
     * @return App\ORM\Filter\FilterAnd Le filtre "FilterAnd" généré par les paramètres donnés
     */
    public static function et($filters): FilterAnd
    {
        return new FilterAnd($filters);
    }

    /**
     * Retourne un filtre "Or" (nommé "Ou" parce que PHP ne veut pas
     * qu'on appelle nos fonctions "ou")
     *
     * @return App\ORM\Filter\FilterOr Le filtre "FilterOr" généré par les paramètres donnés
     */
    public static function ou($filters): FilterOr
    {
        return new FilterOr($filters);
    }

    /**
     * Retourne un filtre "Not"
     * @return App\ORM\Filter\FilterNot Le filtre "FilterNot" généré par les paramètres donnés
     */
    public static function not(string $field, $value): FilterNot
    {
        return new FilterNot($field, $value);
    }
}
