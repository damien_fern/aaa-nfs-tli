<?php

namespace App\ORM;

use App\Database;
use App\ORM\Entity;

/*
 * Classe générique définissant les associations en base de données.
 *
 * Une classe association qui hérite de cette classe doit définir de manière
 * statique :
 * $table_name : Le nom de la table associative en base de données
 * $ids_field_names : Le tableau associatif [k => v] où :
 *  * k : string class_name, le nom de la classe Entity
 *  * v : string id_name, le nom du champ 'ID' qui concerne la classe k
 */
class Association
{
    /**
     * Méthode effectuant l'association, remplissant le champ $associated de
     * l'objet Entity passé en paramètres
     *
     * @param Entity L'entity sur laquelle on va effectuer l'association
     */
    public static function doAssociation(Entity $entity)
    {
        $class_name = get_class($entity);
        $pdo = Database::getInstance();
        $query = $pdo->prepare(
            "SELECT * FROM " . static::$table_name . " WHERE "
            . static::$ids_field_names[$class_name] . " = " . $entity->getId()
        );
        $query->execute();
        $res = $query->fetchAll(\PDO::FETCH_ASSOC);

        // récupère seulement les IDs de l'autre entité
        $ids = array_map(function ($x) use ($class_name) {
            unset($x[static::$ids_field_names[$class_name]]);
            return array_values($x)[0];
        }, $res);

        $cp_field_names = static::$ids_field_names;
        unset($cp_field_names[$class_name]);
        $other_class_name = array_keys($cp_field_names)[0];
        $assoc = $other_class_name::readMultiple($ids);
        $entity->associated = $assoc;
    }
}
