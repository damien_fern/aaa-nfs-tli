<?php

/*
 *  Implémentation basique d'une mini- "couche ORM"
 *
 *  On pose ici deux classes génériques dont le but va être de gérer toute la
 *  logique de requêtes SQL nécéssaire à charger les données de la DB en
 *  évitant la duplication de code et en standardisant les performances
 *  pour les requêtes.
 *
 *  Entity: Représente une table et ses données
 *  Associations: Représente une table associative
 */

namespace App\ORM;

use App\Database;

/*
 * Fonction helper pour formatter une valeur en SQL
 * TODO: mettre dans un fichier common.php pour éviter la duplication entre
 * Filter et Entity
 */
function format_for_sql($x): string
{
    switch (gettype($x)) {
        case "string":
            return "'" . $x . "'";
        default:
            return $x;
    }
}

/*
 * Classe Entite de base, permet de définir une base générique pour
 * l'interaction avec la base de données
 *
 * On créera dans le dossier Model des classes qui héritent de cette classe
 * Entity, qui a pour unique but de définir un comportement générique
 * pour fournir une fonctionnalité de CRUD basique.
 *
 * Chaque entité qui hérite de cette classe doit definir deux variables
 * protected static:
 *      * $table_name: le nom de la table de l'entité en BDD
 *      * $id_name: le nom du champ de l'ID de l'entité en BDD
 */

class Entity
{

    /*
     * Constructeur par défaut de la classe entitée
     */
    public function __construct()
    {
        $this->data = [];
        $this->associated = [];
    }

    /*
     * Fonction permettant de créer une entité dans la base de données.
     * Cette fonction prend la référence sur l'objet et s'en sert pour
     * envoyer la requête en base de données.
     *
     * L'ID est généré par le SGBD cible, puis remplace le champ ID dans
     * les data de cet objet.
     *
     * @return La fonction retourne un booléen: vrai si l'objet a été crée,
     * faux si la création a raté.
     */
    public function create(): bool
    {
        $class_name = get_called_class();
        $pdo = Database::getInstance();

        $fields = $this->data;
        if (isset($fields[static::$id_name])) {
            unset($fields[static::$id_name]);
        }

        $fields_k = implode(",", array_map(function ($x) {
            return static::$table_name . "." . $x;
        }, array_keys($fields)));
        $fields_v = implode(",", array_map(function ($x) {
            return format_for_sql($x);
        }, $fields));

        $query = $pdo->prepare("INSERT INTO " . static::$table_name .
            " (" . $fields_k . ") VALUES (" .
            $fields_v . ")");
        if (!$query->execute()) {
            return false;
        }

        $query = $pdo->prepare("SELECT " . static::$id_name . " FROM " .
            static::$table_name . " ORDER BY " . static::$id_name .
            " DESC LIMIT 1");
        $query->execute();
        $res = $query->fetch(\PDO::FETCH_ASSOC);

        if ($res == null) {
            return false;
        }
        $this->data[static::$id_name] = $res[static::$id_name];

        return true;
    }

    /**
     * Fonction permettant de lire le contenu de la base de données pour
     * une entité voulue.
     *
     * @param int id L'ID de l'élément à lire en base de données
     *
     * @return La fonction retourne une instance de cette classe, content dans
     * son attribut $data le contenu qui a été lu depuis la base de données
     */
    public static function read($id)
    {
        $class_name = get_called_class();
        $pdo = Database::getInstance();

        $query = $pdo->prepare("SELECT *
        FROM " . static::$table_name . "
        WHERE " . static::$id_name . " = :id");

        $query->bindParam(":id", $id);
        $query->execute();

        $res = $query->fetch(\PDO::FETCH_ASSOC);
        if ($res == null) {
            return null;
        }

        $ret = new $class_name();
        $ret->data = $res;
        return $ret;
    }

    /**
     * Lit toutes les entités présentes en base de données pour la table concernée
     *
     * @return array La liste des entités
     */
    public static function readAll(): array
    {
        $class_name = get_called_class();
        $pdo = Database::getInstance();

        $query = $pdo->prepare("SELECT *
        FROM " . static::$table_name);
        $query->execute();

        $res = $query->fetchAll(\PDO::FETCH_ASSOC);
        $assoc = array_map(function ($x) use ($class_name) {
            $v = new $class_name();
            $v->data = $x;
            return $v;
        }, $res);

        return $assoc;

    }

    /**
     * Fonction permettant de lire le contenu de la base de données pour les
     * entités dont l'ID est spécifié dans le tableau d'IDS donné en paramètres
     *
     * @return array La fonction retourne un tableau d'entités
     * Le tableau peut être vide si la requête n'a rien retourné
     */
    public static function readMultiple(array $ids): array
    {
        $class_name = get_called_class();
        $pdo = Database::getInstance();

        $ids_text = "( " . implode(" , ", array_map(function ($x) {
            return ":" . $x;
        }, array_keys($ids))) . " )";
        $query = $pdo->prepare("SELECT *
        FROM " . static::$table_name . "
        WHERE " . static::$id_name . " IN " . $ids_text);
        foreach ($ids as $k => $v) {
            $query->bindValue(':' . $k, $v);
        }
        $query->execute();

        $res = $query->fetchAll(\PDO::FETCH_ASSOC);
        $assoc = array_map(function ($x) use ($class_name) {
            $v = new $class_name();
            $v->data = $x;
            return $v;
        }, $res);

        return $assoc;
    }

    /**
     * Fonction permettant d'effectuer une recherche en utilisant un système
     * de filtres.
     *
     * Un seul filtre doit être passé, on pourra composer la recherche de
     * plusieurs conditions avec un filtre AND ou OR, par exemple.
     *
     * @return array La fonction retourne un tableau d'instances de cette classe.
     */
    public static function searchBy($filter): array
    {
        $class_name = get_called_class();
        $pdo = Database::getInstance();

        $resolved = $filter->resolve();
        $query = $pdo->prepare("SELECT * FROM "
            . static::$table_name .
            " WHERE " . $resolved->sql);
        $resolved->bindValues($query);
        $query->execute();
        $res = $query->fetchAll(\PDO::FETCH_ASSOC);

        $assoc = array_map(function ($x) use ($class_name) {
            $v = new $class_name();
            $v->data = $x;
            return $v;
        }, $res);

        return $assoc;
    }

    /**
     * Fonction permettant de mettre à jour le contenu de la BDD avec
     * les informations contenues dans le champ $data de cet objet.
     *
     * @return bool Vrai si la mise à jour
     * a réussi, Faux si elle a échoué
     */
    public function update(): bool
    {
        $pdo = Database::getInstance();
        $fields = $this->data;
        if (isset($fields[static::$id_name])) {
            unset($fields[static::$id_name]);
        }

        $fields = array_map(function ($k, $v) {
            return static::$table_name . "." . $k . " = " . format_for_sql($v);
        }, array_keys($fields), $fields);
        $query_set = "SET " . implode(" AND ", $fields);

        $query = $pdo->prepare("UPDATE " . static::$table_name . " " .
            $query_set . " WHERE " . static::$id_name . " = :id");
        $query->bindParam(":id", $this->data[static::$id_name]);

        return $query->execute();
    }

    /**
     * Fonction permettant de supprimer le contenu de la BDD associé à
     * l'ID contenue dans le champ $data de cet objet, référencé par le
     * champ avec la clé contenue dans $id_name.
     *
     * @param integer id l'ID de l'élément à supprimer
     *
     * @return La fonction retourne un booléen, vrai si la suppression
     * a réussi, faux si elle a échoué.
     */
    public static function delete($id): bool
    {
        $class_name = get_called_class();
        $pdo = Database::getInstance();

        $query = $pdo->prepare("DELETE
        FROM " . static::$table_name . "
        WHERE " . static::$id_name . " = :id");

        $query->bindParam(":id", $id);
        return $query->execute();
    }

    /**
     * Récupère l'ID en BDD de cette entité.
     *
     * @return l'ID sous forme d'integer
     */
    public function getId()
    {
        return $this->data[static::$id_name];
    }
}
