<?php

namespace App;

/**
 * Class Request
 * @package App
 */
class Request
{
    /**
     * @var array
     */
    private $post;

    /**
     * @var array
     */
    private $get;

    /**
     * @var array
     */
    private $files;

    /**
     * @var array
     */
    private $request;

    /**
     * @var array
     */
    private $server;

    /**
     * Request constructor.
     */
    public function __construct()
    {
        $this->post = $_POST;
        $this->get = $_GET;
        $this->files = $_FILES;
        $this->request = $_REQUEST;
        $this->server = $_SERVER;
    }

    /**
     * Récupère les attributes POST de la requête
     * @return array
     */
    public function getPost()
    {
        return $this->post;
    }

    /**
     * Récupère les attributs GET de la requête
     * @return array
     */
    public function getGet()
    {
        return $this->get;
    }

    /**
     * Récupère l'attribut FILES de la requête
     * @return array
     */
    public function getFiles()
    {
        return $this->files;
    }

    /**
     * Récupère les Cookies associés à la requête
     * @return array
     */
    public function getCookie()
    {
        return $_COOKIE;
    }

    /**
     * Set la valeur d'un cookie associé à cette requête
     */
    public function setCookie($key, $value, $exp)
    {
        setcookie($key, $value, $exp);
    }

    /**
     * Récupère la Session associée à cette requête
     * @return array
     */
    public function getSession()
    {
        return $_SESSION;
    }

    /**
     * Change un attribut de session de la requête
     */
    public function setSession($key, $value)
    {
        $_SESSION[$key] = $value;
    }

    /**
     * Récupère l'attribut de requête associé
     * @return array
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * Récupère l'attribut de serveur associé
     * @return array
     */
    public function getServer()
    {
        return $this->server;
    }

    /**
     * Récupère la variable d'environnement associée à la clé donnée
     * @param $key
     * @return array|false|string
     */
    public function getEnv($key)
    {
        return getenv($key);
    }

    /**
     * Récupère l'URL de la ressource
     * @return string
     */
    public function getUri()
    {
        return $this->server["REQUEST_URI"];
    }
}
