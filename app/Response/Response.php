<?php

namespace App\Response;

/**
 * Class Response
 * @package App\Response
 */
class Response implements ResponseInterface
{
    /**
     * @var string
     */
    private $content;

    /**
     * Response constructor.
     * @param $content
     */
    public function __construct($content)
    {
        $this->content = $content;
    }

    /**
     * Gère l'envoi de la réponse
     */
    public function send()
    {
        echo $this->content;
    }
}
