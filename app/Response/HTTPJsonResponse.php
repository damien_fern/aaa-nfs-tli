<?php

namespace App\Response;

/**
 * Une réponse Json dont le code de status HTTP a été spécifié
 * @package App\Response
 */
class HTTPJsonResponse implements ResponseInterface
{
    /**
     * @var mixed La donnée à renvoyer au format JSON
     */
    private $data;

    /**
     * @var mixed Le status HTTP à renvoyer
     */
    private $code;

    /**
     * Constructeur par défaut
     *
     * @param $data La donnée à sérialiser
     * @param $code Le code HTTP de la réponse
     */
    public function __construct($data, $code)
    {
        $this->data = $data;
        $this->code = $code;
    }

    /*
     * Gère l'envoi de la réponse
     */
    public function send()
    {
        header('Content-type: application/json');
        http_response_code($this->code);
        echo json_encode($this->data);
    }
}
