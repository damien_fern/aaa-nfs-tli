<?php

namespace App\Response;

/**
 *  Une réponse retournant une donnée formatée en JSON, avec pour code HTTP 200
 *  @package App\Response
 */
class JsonResponse implements ResponseInterface
{
    /**
     * @var mixed La donnée à sérialiser
     */
    private $data;

    /**
     * Constructeur par défaut de la classe
     * @param $data Donnée à sérialiser en JSON
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /*
     * Permet de gérer l'envoi d'une réponse
     */
    public function send()
    {
        header('Content-type: application/json');
        echo json_encode($this->data);
    }
}
