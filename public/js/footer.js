var selectElement = document.getElementById("flip-1");

if (localStorage.getItem('highContrast') === null) {
    localStorage.setItem('highContrast', 'styleDark');
}

selectElement.value = localStorage.getItem('highContrast');
selectElement.onchange = function () {
    switchTheme(selectElement.value);
};