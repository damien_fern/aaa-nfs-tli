function switchTheme(themeName) {
    var themeKey = 'highContrast';
    let name = "styleDark";
    if (themeName === null) {
        var storedName = localStorage.getItem('highContrast');
        if (storedName !== null) {
            name = storedName;
        }
    } else {
        name = themeName;
        localStorage.setItem(themeKey, name);
    }
    var link = document.getElementById('themeLink');
    link.setAttribute('href', '/css/' + name + '.css');
}

switchTheme(null);
