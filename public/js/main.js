$(document).ready(function()
{
    //Effectuer la recherche par mots clefs au clique sur le bouton prévu à cet effet
    $("#buttonResearch").on('click', function ()
    {
        searchPathologie()
    });
    $("#buttonGoToAjoutPatho").on("click", function ()
    {
        redirectTo('/pathologieAdd');
    });

    $('body').on('keyup', function (event)
    {
        //Effectuer la recherche par mots clefs si la touche entrée (code 13) est pressée (afin d'améliorer l'accessibilité)
        if ($("#rechercheInput").is(":focus") && event.keyCode == 13)
            searchPathologie();
        if ($("#resetFilter").is(":focus") && event.keyCode == 13)
            redirectTo("/pathologies");
    });
    checkForm();
    filterPathologies();

    $("#resetFilter").on("click", function ()
    {
        redirectTo("/pathologies");
    })

    canvas();

    filterOnClickOnTh();

    displayTriangleOfTri();



});//Fin document.ready

//rechercher toutes les pathologies qui sont associés au mot clef renseigné
function searchPathologie()
{

    var test = "";
    var nbCharToCheck = 2;
    var rechercheValue = $("#rechercheInput").val();

    if($(".alertError").length > 0)
    {
        $(".alertError").remove();
    }
    if (rechercheValue.length>=nbCharToCheck)
    {
        $.ajax
        ({
            url: "/pathologies/findByKeyword",
            type : 'POST',
            data: {rechercheValue: rechercheValue},
            success : function(code_html, statut)
            {
                var pathologies = JSON.parse(code_html);
                console.log(pathologies);


                $.each(pathologies, function ( index, pathologie)
                {
                    test += "<tr>\n" +
                    "            <td data-label=\"ID\">"+pathologie.idP+"</td>\n" +
                    "            <td data-label=\"Meridien\">"+pathologie.nomMeridien+"</td>\n" +
                    "            <td data-label=\"Type\">"+pathologie.typePatho+"</td>\n" +
                    "            <td data-label=\"Description\">"+pathologie.descPatho+"</td>\n" +
                    "            <td data-label=\"Detail\"><a href=\"/pathologies/"+pathologie.idP+"\">Voir le detail</a></td>\n" +
                    "        </tr>";
                });

                $("#tbodyPathologies").html('');
                $("#tbodyPathologies").append(test);
            }
        });
    }
    else
    {
        $("#tbodyPathologies").html('');
        $("table").after("<div class='row flexCenter alertError'><p>Veuillez remplir la recherche avec au moins "+nbCharToCheck+" caractères</p></div>");
    }
}

function filterPathologies()
{
    $("#meridienListFilter, #typeListFilter, #descriptionListFilter").on('focusout change', function ()
    {
        var meridien = $("#meridienListFilter").val();
        var type = $("#typeListFilter").val();
        var description = $("#descriptionListFilter").val();
        var test = "";

        var data =
            {
                mer:meridien,
                type:type,
                desc:description

            };
        $.ajax
        ({
            url: "/pathologies/findByFilters",
            type : 'GET',
            data: {data: data},
            success : function(code_html, statut)
            {
                var pathologies = JSON.parse(code_html);


                $.each(pathologies, function ( index, pathologie)
                {

                    test += "<tr>\n" +
                        "            <td scope=\"row\" data-label=\"ID\">"+pathologie.idP+"</td>\n" +
                        "            <td data-label=\"Meridien\">"+pathologie.mer+"</td>\n" +
                        "            <td data-label=\"Type\">"+pathologie.typePatho+"</td>\n" +
                        "            <td data-label=\"Description\">"+pathologie.descPatho+"</td>\n" +
                        "            <td data-label=\"Detail\"><a href=\"/pathologies/"+pathologie.idP+"\">Voir le detail</a></td>\n" +
                        "        </tr>";
                });

                $("#tbodyPathologies").html('');
                $("#tbodyPathologies").append(test);
            }
        });
    });
}


function redirectTo(url)
{
    window.location=''+url+'';
}

function isValidEmailAddress(emailAddress) {
    var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
    return pattern.test(emailAddress);
}

function checkForm()
{
    $(':input[required]:visible').on("focusout",function ()
    {
        var thisInput = $(this);
        var value = thisInput.val();
        var type = thisInput.attr('type');
        var id = thisInput.attr('id');
        var textLabel =  $('#'+id+'').parent().find('label').text();
        var message = textLabel.substring(0,textLabel.length-2);

        if (value === "" ||value === null )
        {
            if( $('#error'+id+'').length == 0)
                thisInput.after('<p id="error'+id+'" class="alertDanger loginError">Veuillez renseigner votre '+message+'</p>');
        }
        else if (type == "email" && !isValidEmailAddress(value) )
        {
            thisInput.after('<p id="emailError'+id+'" class="alertDanger loginError">Veuillez renseigner un email valide (exemple@exemple.com)</p>');
            if( $('#error'+id+'').length >0)
                $('#error'+id+'').remove();
        }
        else
        {
            if( $('#error'+id+'').length >0)
                $('#error'+id+'').remove();
            if( $('#emailError'+id+'').length >0)
                $('#emailError'+id+'').remove();

        }
    });
}

function canvas()
{
    if (document.title == "AAA NFS TLI - Accueil")
    {
        var canvas  = document.querySelector('#canvas');
        var context = canvas.getContext('2d');

        // context.fillStyle = "rgba(215, 175, 0, 0)";
        // context.fillRect(60, 50, 100, 70);

        context.font = "9em Clicker Script";
        context.fillStyle = "#d7af00";
        context.fillText("NFS", 0, 120);
    }

}
function filterOnClickOnTh()
{
    var triSens = "ASC";
    var url = document.URL;


    if (url.includes("ASC"))
    {
        triSens = "DESC";
    }
    else if(url.includes("DESC"))
    {
        triSens = "ASC";
    }
    else
    {
        triSens = "ASC";
    }

    $("#ligneTitreFilter th").on('click', function ()
    {
        var triColonne = "";
        if ($(this).prop("tagName") == "TH")
            triColonne = $(this).data("triname");
        else if ($(this).prop("tagName") == "LABEL")
            triColonne = $(this).parent().data("triname");

        // console.log("/pathologies?triColonne="+triColonne+"&triSens="+triSens);
        redirectTo("/pathologies?triColonne="+triColonne+"&triSens="+triSens)


    });



}

function displayTriangleOfTri()
{

    var url = document.URL;
    var tricolonne = url.substring(
        url.lastIndexOf("triColonne=")+11,
        url.lastIndexOf("&")
    );
    var triangleUp = "&#9650;";
    var triangleDown = "&#9660;";
    var element = $("th[data-triName='" + tricolonne +"']");

    if (url.includes("ASC"))
        element.append(triangleUp);
    else
        element.append(triangleDown)


}